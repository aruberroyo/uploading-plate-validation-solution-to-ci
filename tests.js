test("Valid1", function (assert) {
    assert.equal(isValidPlate("0000BBB"), true, "Valid");
});
test("Valid2", function (assert) {
    assert.equal(isValidPlate("1111CDF"), true, "Valid");
});
test("Valid3", function (assert) {
    assert.equal(isValidPlate("2222GHJ"), true, "Valid");
});
test("Valid4", function (assert) {
    assert.equal(isValidPlate("3333KLM"), true, "Valid");
});
test("Valid5", function (assert) {
    assert.equal(isValidPlate("4444NPR"), true, "Valid");
});
test("Valid6", function (assert) {
    assert.equal(isValidPlate("5555STV"), true, "Valid");
});
test("Valid7", function (assert) {
    assert.equal(isValidPlate("6666WXY"), true, "Valid");
});
test("Valid8", function (assert) {
    assert.equal(isValidPlate("7777ZRB"), true, "Valid");
});
test("Valid9", function (assert) {
    assert.equal(isValidPlate("8888JLV"), true, "Valid");
});
test("Valid10", function (assert) {
    assert.equal(isValidPlate("9999WWF"), true, "Valid");
});
test("Valid11", function (assert) {
    assert.equal(isValidPlate("1234PCC"), true, "Valid");
});
test("Valid12", function (assert) {
    assert.equal(isValidPlate("5678YTJ"), true, "Valid");
});
test("Valid13", function (assert) {
    assert.equal(isValidPlate("9191HBL"), true, "Valid");
});
test("Valid14", function (assert) {
    assert.equal(isValidPlate("2468KNC"), true, "Valid");
});
test("Valid15", function (assert) {
    assert.equal(isValidPlate("1932ZGZ"), true, "Valid");
});
test("Valid16", function (assert) {
    assert.equal(isValidPlate("7412ZNP"), true, "Valid");
});
test("Valid17", function (assert) {
    assert.equal(isValidPlate("1002SSV"), true, "Valid");
});
test("Valid18", function (assert) {
    assert.equal(isValidPlate("6324RHV"), true, "Valid");
});
test("Valid19", function (assert) {
    assert.equal(isValidPlate("7052NJH"), true, "Valid");
});
test("Valid20", function (assert) {
    assert.equal(isValidPlate("1122MNM"), true, "Valid");
});
test("NoValid21", function (assert) {
    assert.equal(isValidPlate("2434Bys"), true, "Valid");
});
test("NoValid22", function (assert) {
    assert.equal(isValidPlate("8707LRc"), true, "Valid");
});
test("NoValid23", function (assert) {
    assert.equal(isValidPlate("2567VdY"), true, "Valid");
});
test("NoValid24", function (assert) {
    assert.equal(isValidPlate("1245CTf"), true, "Valid");
});
test("NoValid25", function (assert) {
    assert.equal(isValidPlate("7512fcf"), true, "Valid");
});
test("NoValid1", function (assert) {
    assert.equal(isValidPlate("BCDF123"), false, "No Valid");
});
test("NoValid2", function (assert) {
    assert.equal(isValidPlate("D123CBR"), false, "No Valid");
});
test("NoValid3", function (assert) {
    assert.equal(isValidPlate("4J23PNC"), false, "No Valid");
});
test("NoValid4", function (assert) {
    assert.equal(isValidPlate("97Y4WLP"), false, "No Valid");
});
test("NoValid5", function (assert) {
    assert.equal(isValidPlate("025XPÑL"), false, "No Valid");
});
test("NoValid6", function (assert) {
    assert.equal(isValidPlate("1269245"), false, "No Valid");
});
test("NoValid7", function (assert) {
    assert.equal(isValidPlate("63683FG"), false, "No Valid");
});
test("NoValid8", function (assert) {
    assert.equal(isValidPlate("ASFWQRT"), false, "No Valid");
});
test("NoValid9", function (assert) {
    assert.equal(isValidPlate("5942VT7"), false, "No Valid");
});
test("NoValid10", function (assert) {
    assert.equal(isValidPlate("6345AEI"), false, "No Valid");
});
test("NoValid11", function (assert) {
    assert.equal(isValidPlate("0854AVC"), false, "No Valid");
});
test("NoValid12", function (assert) {
    assert.equal(isValidPlate("3424SEV"), false, "No Valid");
});
test("NoValid13", function (assert) {
    assert.equal(isValidPlate("1235HJI"), false, "No Valid");
});
test("NoValid14", function (assert) {
    assert.equal(isValidPlate("3256QRT"), false, "No Valid");
});
test("NoValid15", function (assert) {
    assert.equal(isValidPlate("7454BÑP"), false, "No Valid");
});
test("NoValid16", function (assert) {
    assert.equal(isValidPlate("124QPCN"), false, "No Valid");
});
test("NoValid17", function (assert) {
    assert.equal(isValidPlate("3252AQL"), false, "No Valid");
});
test("NoValid18", function (assert) {
    assert.equal(isValidPlate("4132NVQ"), false, "No Valid");
});
test("NoValid19", function (assert) {
    assert.equal(isValidPlate("7567ANC"), false, "No Valid");
});
test("NoValid20", function (assert) {
    assert.equal(isValidPlate("2352asd"), false, "No Valid");
});
test("NoValid21", function (assert) {
    assert.equal(isValidPlate("2434biñ"), false, "No Valid");
});
test("NoValid22", function (assert) {
    assert.equal(isValidPlate("8071+WR"), false, "No Valid");
});
test("NoValid23", function (assert) {
    assert.equal(isValidPlate("1745ev"), false, "No Valid");
});
test("NoValid24", function (assert) {
    assert.equal(isValidPlate("5867iii"), false, "No Valid");
});
test("NoValid25", function (assert) {
    assert.equal(isValidPlate("9452K4W"), false, "No Valid");
});